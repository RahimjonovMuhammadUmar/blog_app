package main

import (
	"fmt"
	"log"

	"github.com/go-redis/redis/v9"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"gitlab.com/t.mannonov/blog_app/api"
	"gitlab.com/t.mannonov/blog_app/config"
	"gitlab.com/t.mannonov/blog_app/storage"
	_ "golang.org/x/lint"
)

func main() {
	// Loads all configs
	cfg := config.Load(".")

	// Connects to postgres
	psqlURL := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)
	// Connects to sqlx
	psqlConn, err := sqlx.Connect("postgres", psqlURL)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}
	// connects to redis
	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Addr,
	})

	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)

	apiServer := api.New(&api.RouterOptions{
		Cfg:      &cfg,
		Storage:  strg,
		InMemory: inMemory,
	})

	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}

	log.Print("Server stopped")
}
